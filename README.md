# Faride ILANDA
## DIC 3 Informatique / DGI / ESP

## ionic-project
App de restauration.
Visualisation des menus et plats sur le map.

## How to launch the app:

## 1.)  Clonner le projet en local

## 2.)  Se mettre dans le dossier 'restaurant' ensuite exécuter les commandes :
###         $ npm install 
###         $ npm update
 
## 3.)  Créer un projet 'strapi' via la commande :
###         $ strapi new <project_name> --quickstart
### ensuite créer les beans (les tables) restaurant, plat en se basant les fichiers json/js contenus dans le dossier 'strapi/api
### ou soit copier le dossier 'api' en question et copier et remplacer par celui qui s'y trouve dans le projet 'project_name' que vous avez créé.
### la création des tables terminée, lancer l'api via la commande :
###         $ strapi start

## 4.)  Revenir sur le projet ionic 'restaurant'
### lancer le projet via la commande :
###         $ ionic serve
###           link: http://localhost:8100/
###         $ ionic serve --lab
###           link: http://localhost:8200/

## 5.)  Le Build de l'apk android
### vous devez disposer de 'Android Studio', du 'JDK 1.8.*', installer les platform android sur ionic enfin faire :
###         $ ionic cordova build --release android